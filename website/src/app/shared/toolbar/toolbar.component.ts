import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  @Input() open: boolean = false;
  @Output() openChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  scrolled: boolean = false;
  initial: boolean = true;
  top: boolean = true;

  constructor() { }

  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll(event: any): void {
    this.initial = false;

    if (window.scrollY > 0) {
      this.scrolled = true;
      this.top = false;
    } else {
      this.scrolled = false;
      this.top = true;
    }
  }

  toggleSidebar(): void {
    this.open = !this.open;
    this.openChange.emit(this.open);
  }
}
