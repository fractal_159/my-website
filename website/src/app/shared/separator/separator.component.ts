import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-separator',
  template: '<div class="app-separator"><hr></div>',
  styleUrls: ['./separator.component.scss']
})
export class SeparatorComponent {

  constructor() { }

}
