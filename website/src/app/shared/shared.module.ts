import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SidebarContentComponent } from './sidebar-content/sidebar-content.component';
import { MatDividerModule } from '@angular/material/divider';
import { SeparatorComponent } from './separator/separator.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ToolbarComponent,
    NotFoundComponent,
    SidebarContentComponent,
    SeparatorComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    AngularSvgIconModule,
    RouterModule
  ],
  exports: [
    ToolbarComponent,
    NotFoundComponent,
    SidebarContentComponent
  ]
})
export class SharedModule { }
