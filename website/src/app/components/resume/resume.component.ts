import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

export interface ResumeCard {
  cols: number;
  rows: number;
  title: string;
  subtitle?: string;
  content: ResumeContent[];
}

export interface ResumeContent {
  bold?: string;
  italic?: string;
  date?: string;
  bullets?: string[];
}

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent {

  resumeInfo: ResumeCard[];

  constructor(
    private title: Title
  ) {
    this.title.setTitle('Chandler Mitchell | Resume')

    this.resumeInfo = [
      this.education(),
      this.skillsAndStrengths(),
      this.activities(),
      this.currentEmployment(),
      this.interests(),
      this.previousEmployment()
    ];
  }

  education(): ResumeCard {
    return {
      cols: 4,
      rows: 4,
      title: 'Educaiton',
      subtitle: 'B.S. Dec 2020',
      content: [
        { bold: 'Colorado School of Mines', italic: 'Golden, CO', bullets: ['Computer Science', 'Major GPA: 4.00', 'Overall GPA: 3.98'] }
      ]
    }
  }

  currentEmployment(): ResumeCard {
    return {
      cols: 4,
      rows: 5,
      title: 'Current Employment',
      subtitle: 'Since Feb 2021',
      content: [
        { bold: 'Software Engineer', italic: 'Lockheed Martian', bullets: ['Angular Web Development', 'JavaEE Developmet', 'Spring Boot Development', 'CI/CD with Jenkins and Ansible', 'Agile Team', 'Security Clearance: TS/SCI'] }
      ]
    }
  }

  skillsAndStrengths(): ResumeCard {
    return {
      cols: 3,
      rows: 9,
      title: 'Skills & Strengths',
      content: [
        { bold: 'Programming Lenguages', bullets: ['Java', 'C++', 'JavaScript', 'Python'] },
        { bold: 'Software Engineering', bullets: ['Agile Development', 'Test Driven Development', 'Pair Programming'] },
        { bold: 'Software Focus', bullets: ['Angular', 'React', 'Shell Scripting', 'Unix', 'Git', 'MATLAB', 'SolidWorks', 'AutoCAD'] }
      ]
    }
  }

  interests(): ResumeCard {
    return {
      cols: 3,
      rows: 6,
      title: 'Interests',
      content: [
        { bold: 'Outdoors', bullets: ['Skiing', 'Camping', 'Hiking', 'Fishing', 'Mountain Biking'] },
        { bold: 'Computers', bullets: ['Programming', 'Microcomputers', 'Building',] }
      ]
    }
  }

  activities(): ResumeCard {
    return {
      cols: 3,
      rows: 6,
      title: 'Activities',
      content: [
        { bold: 'Tau Beta Pi Engineering Honor Society', bullets: ['Member'] },
        { bold: 'CSM: The Gathering Club', bullets: ['Club President'] },
        { bold: 'Boy Scouts', bullets: ['Eagle Scout'] },
        { bold: 'BSA Venturing', bullets: ['Crew President'] }
      ]
    }
  }

  previousEmployment(): ResumeCard {
    return {
      cols: 7,
      rows: 8,
      title: 'Previous Employment & Relevant Experience',
      content: [
        { bold: 'Software Engineering Intern', italic: 'Lockheed Martian', date: 'June 2020 - Aug 2020', bullets: ['Migration to a new front end utilizing React.', 'Creation of system documentation for reference and new developer training.', 'Planning the next quarter of Agile Development.'] },
        { bold: 'Physics 200 Teaching Assistant', italic: 'Colorado School of Mines', date: 'Aug 2018 - Dec 2020', bullets: ['Lead lab sections which entails assisting 140 students, answering questions, and ensuring the safe operation of power supplies, capacitors, and other electronics.', 'Hold weekly office hours in support of student questions.', 'Assist in the grading of homework and exams.'] },
        { bold: 'Peer Mentor', italic: 'Colorado School of Mines', date: 'Aug 2018 - Dec 2018' },
        { bold: 'Cashier', italic: 'King Soopers', date: 'July 2016 - Aug 2018' },
        { bold: 'Eagle Scout Project', italic: 'Highlands Ranch Community Association', date: 'July 2016 - Aug 2018' }
      ]
    }
  }
}
