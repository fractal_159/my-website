import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnChanges {

  backgroundImg: string = '';
  private images: string[] = ['coast_small', 'chateau_path', 'chateau_bridge', 'florence'];


  @Input() open: boolean = false;
  @Output() openChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
    this.backgroundImg = `url(/assets/img/backgrounds/${this.images[this.randomInt(0, this.images.length - 1)]}.png)`
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['open'] && changes['open'].currentValue !== changes['open'].previousValue) {
      this.openChange.emit(this.open);
    }
    console.log(this.open);
  }

  private randomInt(min: number, max: number): number {
    const max_int = Math.floor(max);
    const min_int = Math.ceil(min);
    return Math.floor(Math.random() * (max_int - min_int) + min_int);
  }
}
