#!/bin/bash

cd /tmp
mkdir website
tar xf compiled_website_*.tar.gz -C /tmp/website
cd /tmp/website/builds/fractal_159/my-website/website/dist/website/
rm -f /var/www/html/*
cp -r ./* /var/www/html
rm -f /var/www/html/favicon.ico
rm -rf /tmp/website
rm -f /tmp/compiled_website_*.tar.gz
rm -f /tmp/deploy_website.sh
